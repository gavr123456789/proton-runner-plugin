/* ProjectManager.vala
 *
 * Copyright 2019 Paulo Queiroz <pvaqueiroz@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

namespace Runner
{
    public errordomain AssertionError
    {
        ONLY,
    }

    public errordomain PMError
    {
        INVALID_BUILDSYSTEM,
    }

    public void assert_(bool cond, string msg) throws AssertionError
    {
        if (!cond) throw new AssertionError.ONLY(msg);
    }
}

public class Runner.ProjectManager : Object
{
    // Properties
    public Proton.File root { get; private set; }
    public Runner.Plugin plug { get; private set; }
    public IPipeline? pipeline { get; private set; default = null; }

    public bool can_play {
        get {
            return (this.pipeline != null);
        }
    }

    public bool is_playing {
        get {
            if (this.pipeline == null) return (false);
            return (this.pipeline.is_playing);
        }
    }

    public ProjectManager(Runner.Plugin plug)
    {
        this.plug = plug;
        this.root = plug.window.root;
    }

    // Search this.root for .proton.json and validate the buildsystem
    public async bool scan()
    {
        string fname = this.root.path + "/.proton.json";

        if (!FileUtils.test(fname, FileTest.IS_REGULAR))
        {
            message("[Runner] no project configuration file found");
            return (false);
        }

        var f = new Proton.File(fname);

        try
        {
            Json.Parser p;
            Json.Node?  n;
            Json.Object root;
            string? s = yield f.read_async();

            assert_(s != null, "Could not read .proton.json");

            p = new Json.Parser();
            p.load_from_data(s);

            n = p.get_root();

            assert_(n != null, "Could not parse JSON from .proton.json");
            assert_(n.get_node_type() == Json.NodeType.OBJECT,
                    ".proton.json JSON root must be an object");

            root = n.get_object();

            n = root.get_member("buildsystem");

            assert_(n != null, ".proton.json doesn't have key 'buildsystem'");
            assert_(n.get_node_type() == Json.NodeType.VALUE,
                    ".proton.json 'buildsystem' must be a string");

            this.handle_buildsystem(n, root);
        }
        catch (Error e)
        {
            warning("[Runner] %s", e.message);
            return (false);
        }

        return (true);
    }

    private void handle_buildsystem(Json.Node n, Json.Object root) throws Error
    {
        string? bs = n.get_string();

        switch (bs)
        {
            case "simple":
            {
                var p = new SimplePipeline(this);
                p.setup(root);

                this.pipeline = p;
                break;
            }
            case "meson":
            {
                var p = new MesonPipeline(this);
                p.setup(root);

                this.pipeline = p;
                break;
            }
            default: throw new PMError.INVALID_BUILDSYSTEM(
                "%s is not a valid buildsystem", bs);
        }
    }
}
