/* IPipeline.vala
 *
 * Copyright 2019 Paulo Queiroz <pvaqueiroz@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

public interface Runner.IPipeline : Object
{
    public abstract bool          is_playing { get; protected set; }
    public abstract Proton.File   root       { get; protected set; }
    public abstract Runner.Plugin plug       { get; protected set; }

    // This is used to keep track of what's being done. In meson, for exmaple,
    // this would fire with "building", "installing", "running", etc.
    public signal void doing(string something);
    public signal void finished(int status);

    public abstract void setup(Json.Object root) throws Error;
    public abstract void kill();
    public abstract void start();
}
